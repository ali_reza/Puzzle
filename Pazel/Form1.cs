﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Pazel
{
    public partial class Form1 : Form
    {
        public static Bitmap File;
        int pz_x = 3;
        int pz_y = 3;

        int win_he = 570;
        int win_wi = 1000;
        int free = 0;
        Random rand = new Random();
        public static Bitmap flag = new Bitmap(150, 150);
        Brush bgcolor = new SolidBrush(Color.FromArgb(0, 0, 0));
        Graphics flagGraphics = Graphics.FromImage(flag);

        int[] wher;
        public Form1()
        {
            InitializeComponent();
        }

        private Bitmap split(int[] Start, int[] End, Bitmap ima)
        {
            progress progress = new progress();
            progress.Show();
            progress.progressBar.Minimum = 1;
            progress.progressBar.Maximum =  (End[0] - Start[0]) * (End[1] - Start[1]);
            progress.progressBar.Step = 1;
            Bitmap Out = new Bitmap(End[0] - Start[0], End[1] - Start[1]);
            Graphics Graphics = Graphics.FromImage(Out);
            int lon = (End[0] - Start[0]) / 5 ;
            for (int x = Start[0]; x < End[0]; x++)
            {
                for (int y = Start[1]; y < End[1]; y++)
                {
                    try
                    {
                        Brush plus_Color = new SolidBrush(ima.GetPixel(x, y));
                        Graphics.FillRectangle(plus_Color, x - Start[0], y - Start[1], 1, 1);
                        progress.progressBar.PerformStep();
                    }
                    catch (Exception)
                    {
                    }
                }

                if (x % lon == 0)
                {
                    progress.PictureBox.Image = Out;
                    progress.PictureBox.Refresh();
                }
            }
            progress.Close();
            return Out;

        }

        public int[] number_to_xy(int num, int size)
        {
            //y , x
            int[] xy = { 0, 0 };

            for (int i = 1; i <= num; i++)
            {
                if (i % (size / 60) == 0)
                {
                    xy[0]++;
                }
            }

            xy[1] = num - (xy[0] * (size / 60));
            return xy;

        }

        private void setGameImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog File_Dialog = new OpenFileDialog();
            File_Dialog.ShowDialog();
            if (File_Dialog.FileName != "")
            {
                Image a = Image.FromFile(File_Dialog.FileName);
                File = new Bitmap(a);
                int[] start;
                int[] end;
                if (File.Width > File.Height)
                {
                    start = new int[] { 0, 0 };
                    end = new int[] { File.Height, File.Height };
                }
                else
                {
                    start = new int[] { 0, 0 };
                    end = new int[] { File.Width, File.Width };
                }
                File = split(start, end, File);

                showImageToolStripMenuItem.Enabled = true;
                startGameToolStripMenuItem.Enabled = true;
            }
        }

        void Click(object sender, EventArgs e)
        {
            int si = win_he / pz_x;
            PictureBox PictureBox = (PictureBox)(sender);
            int yy = PictureBox.Top / si;
            int xx = PictureBox.Left / si;
            int pos = ((yy * pz_x) + xx);
            if (pos + 1 == free)
            {
                PictureBox.Left = (xx + 1) * si;
                PictureBox.Top = yy * si + 24;
                wher[free] = ((yy * pz_x) + (xx + 1));
                free = pos;
                wher[pos] = free;
            }
            else if (pos - 1 == free)
            {
                PictureBox.Left = (xx - 1) * si;
                PictureBox.Top = yy * si + 24;
                wher[free] = ((yy * pz_x) + (xx - 1));
                free = pos;
                wher[pos] = free;
            }
            else if (pos + pz_y == free)
            {
                PictureBox.Left = xx * si;
                PictureBox.Top = (yy + 1) * si + 24;
                wher[free] = (((yy + 1) * pz_x) + (xx));
                free = pos;
                wher[pos] = free;
            }
            else if (pos - pz_y == free)
            {
                PictureBox.Left = xx * si;
                PictureBox.Top = (yy - 1) * si + 24;
                wher[free] = (((yy - 1) * pz_x) + (xx));
                free = pos;
                wher[pos] = free;
            }

            int ok = 0;



            for (int i = 0; i < wher.Length; i++)
                if (wher[i] == i)
                    ok++;
           
            if (ok == wher.Length)
            {
                MessageBox.Show("Win!!");
            }


        }

        private void startGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (File != null)
            {
                int si = win_he / pz_x;
                int si_cut = File.Width / pz_x;
                this.Height = si * pz_x + 100;
                this.Width = si * pz_y;
                wher = new int[pz_x * pz_y];
                int wher_len = 0;

                //x
                for (int i = 0; i < pz_x; i++)
                {
                    //y
                    for (int ii = 0; ii < pz_y; ii++)
                    {
                        if (ii + 1 == pz_y && i + 1 == pz_x)
                            continue;

                        PictureBox PictureBox = new PictureBox();
                        PictureBox.Size = new Size(si, si);
                        PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                        this.Controls.Add(PictureBox);
                        int[] start = { ii * si_cut, i * si_cut };
                        int[] end = { start[0] + si_cut, start[1] + si_cut };
                        PictureBox.Image = split(start, end, File);

                   
                        PictureBox.Click += new EventHandler(Click);

                        while (true)
                        {
                            int xx = rand.Next(0, pz_x);
                            int yy = rand.Next(0, pz_y);
                            int ok = 0;
                            for (int iii = 0; iii < wher.Length; iii++)
                                if ((yy * pz_x) + xx != wher[iii])
                                    ok++;

                            if (ok == wher.Length)
                            {
                                PictureBox.Left = xx * si;
                                PictureBox.Top = yy * si + 24;
                                wher[wher_len] = (yy * pz_x) + xx;
                                wher_len++;
                                break;
                            }
                        }
                    }

                }
                wher[free] = free;

            }
            else
                MessageBox.Show("Plz Select Image | Seting -> Set Game Image", "Warn", MessageBoxButtons.OK, MessageBoxIcon.Warning);

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new About().Show();
        }

        private void showImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (File != null)
            {
                new Help().Show();
            }
            else
            {
                MessageBox.Show("Plz Select Image | Seting -> Set Game Image", "Warn", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            showImageToolStripMenuItem.Enabled = false;
            startGameToolStripMenuItem.Enabled = false;
        }

        private void toolStripTextBox2_TextChanged(object sender, EventArgs e)
        {

            Match match = Regex.Match(toolStripTextBox2.Text, @"^[0-9]*$");
           
            if (match.Success)
                pz_x = int.Parse(toolStripTextBox2.Text);
            else
                toolStripTextBox2.Text = pz_x.ToString();
             

        }

        private void toolStripTextBox3_TextChanged(object sender, EventArgs e)
        {
            Match match = Regex.Match(toolStripTextBox3.Text, @"^[0-9]*$");

            if (match.Success)
                pz_y = int.Parse(toolStripTextBox3.Text);
            else
                toolStripTextBox3.Text = pz_y.ToString();
        }

        private void setGameSizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripTextBox2.Text = pz_x.ToString();
            toolStripTextBox3.Text = pz_y.ToString();
        }

        private void setingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripTextBox2.Text = pz_x.ToString();
            toolStripTextBox3.Text = pz_y.ToString();
        }
    }
}
